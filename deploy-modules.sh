# Copyright (C) 2023 za267@hotmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################
#!/bin/bash

# TODO: Remove all of the hardcoded paths.

DEV_PATH="/home/someguy/aurelium-dev/minetest_data"

rsync -avz ./mods $DEV_PATH/.minetest/

# Generate world.mt file
cat /home/someguy/secret > $DEV_PATH/.minetest/worlds/world/world.mt
echo '# Module List #' >> $DEV_PATH/.minetest/worlds/world/world.mt
cat world.mt.mods >> $DEV_PATH/.minetest/worlds/world/world.mt

## CUSTOM MODULE MANIPULATION ######################################
## This section is for handling non-standard install directories.
####################################################################
# 1) Terumet is a pain.
## Clean up previous deploy dir if exists
if [ -d $DEV_PATH/.minetest/mods/terumet.deploy ]; then
  echo "Dir was not deleted on previous deploy..."
  rm -Rf $DEV_PATH/.minetest/mods/terumet.deploy
fi
mv $DEV_PATH/.minetest/mods/terumet $DEV_PATH/.minetest/mods/terumet.deploy
mv $DEV_PATH/.minetest/mods/terumet.deploy/terumet $DEV_PATH/.minetest/mods/
rm -Rf $DEV_PATH/.minetest/mods/terumet.deploy
# 2) Ethereal needs some customization
## TODO add ethereal customizations.

## Add soft dependency of prefab_redo to signs_lib
if [ -f $DEV_PATH/.minetest/mods/signs_lib/depends.txt ]; then
  echo "prefab_redo?" >> $DEV_PATH/.minetest/mods/signs_lib/depends.txt
fi

# Final step: Set permissions
podman unshare chown -R 30000: $DEV_PATH/.minetest/
chown -R :someguy $DEV_PATH/.minetest/
#sudo restorecon -RFv $DEV_PATH/.minetest/
